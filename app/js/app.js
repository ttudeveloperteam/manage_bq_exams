'use strict';
angular.module('manageBqExams', ['locale', 'manageExamsView', 'mainVedemostView', 'simplePopup', 'DataService', 'dropdownSelect', 'scaleToFit'])
    .controller('MainController', ['$scope', '$rootScope', '$location', '$window', '$timeout', 'DataService', 'locale',
        /**
         *
         * @param $scope
         * @param $location
         * @param DataService
         */
            function ($scope, $rootScope, $location, $window, $timeout, DataService, locale) {
            var handleNotification = function (name, data) {
                switch (name) {
                    case MessageConstants.ALL:
                        DataService.initConfig(data);
                        break;
                }
            };

            function showInvalidTicketPopup(){
                $rootScope.$emit('open_ng_popup', 'invalidTicketPopup');
            }

            function init() {
                var params = DataService.getParams();
                if (params[0] && params[1]) {
                    var config = new TeacherConfigVO();
                    config.teacherId = params[0];
                    config.teacherName = params[1];
                    config.teacherType = params[2];
                    config.teacherTime = params[3];
                    config.teacherFaculty = JSON.parse(params[4]);
                    config.uchebYear = +params[5];
                    handleNotification(MessageConstants.ALL, config);
                } else {
                    $timeout(showInvalidTicketPopup, 1000);
                }
            }

            /**
             * Remove component handler
             */
            function onDestroy() {
                $window.onbeforeunload = null;
                onDestroy = null;
                handleNotification = null;
                try {
                    $rootScope.$destroy();
                } catch (e) {
                    console.log(e);
                }
            }

            /**
             *  Listen to page unload event
             */
            $window.onbeforeunload = onDestroy;
            init();
        }]);
