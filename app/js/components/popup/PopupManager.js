angular.module('simplePopup', [])
    .controller('PopupController', ['$scope', '$sniffer', '$element', '$window', '$rootScope', '$compile', '$http', '$timeout', function ($scope, $sniffer, $element, $window, $rootScope, $compile, $http, $timeout) {
        var
            /**
             *
             * @type {{deleteQuestion: string, editSingle: string}}
             */
            tpl = {
                deleteVariantPopup: '<delete-variant-popup></delete-variant-popup>',
                deleteTestPopup: '<delete-test-popup></delete-test-popup>'
            },

            /**
             *
             */
            isFullScreenPopup = false,
            timeoutId,

            /**
             *
             * @type {Array}
             */
            watchers = [],

            /**
             *
             * @type {*}
             */
            win = angular.element($window);

        /**
         * Handler for close event sent by close button of popup
         * @param str
         */
        function onClosePopupHandler() {
            $scope.hidePopup();
        }


        /**
         * Handler for open popup notification
         * @param name Notification name
         * @param data Notification data
         * @param message Error message
         */
        function onOpenPopupHandler(name, data, message, isFullScreen) {
            $scope.message = message;
            isFullScreenPopup = isFullScreen;
            $element.html("");
            $element.append($compile(tpl[data])($scope));
            showPopup();
        }

        /**
         * Hides popup
         */
        $scope.hidePopup = function () {
            $element.addClass('hidden');
        };

        /**
         * Hides popup
         */
        $scope.onSubmitDeleteTest = function () {
            $rootScope.$emit('submitDelete');
            $scope.hidePopup();
        };

        /**
         *
         */
        $scope.onSubmitDeleteVariant = function () {
            $rootScope.$emit('deleteVariantConfirm');
            $scope.hidePopup();
        };
        /**
         * Resizes popup
         */
        $scope.resizePopup = function () {
            if (timeoutId) {
                $timeout.cancel(timeoutId);
            }
            var popupHolder = $element.find('div')[0],
                ngpopup = $element.find('div')[1];
            if (ngpopup && popupHolder) {
                var ww = $window.innerWidth,
                    wh = $window.innerHeight,
                    nw = ngpopup.clientWidth,
                    nh = ngpopup.clientHeight;
                if (isFullScreenPopup) {
                    ngpopup.style.width = ww + 'px';
                    ngpopup.style.height = wh + 'px';
                    popupHolder.style.top = '0';
                    popupHolder.style.left = '0';
                } else {
                    popupHolder.style.top = (wh - nh) / 2 + 'px';
                    popupHolder.style.left = (ww - nw) / 2 + 'px';
                }
            }
        };

        /**
         * Shows the popup
         */
        function showPopup() {
            $element.removeClass('hidden');
            timeoutId = $timeout($scope.resizePopup, 5);
        }

        /**
         * Clean up method
         */
        function onDestroy() {
            onOpenPopupHandler = null;
            showPopup = null;
            onDestroy = null;

            win.off('resize', $scope.resizePopup);
            for (var key in $scope) {
                if (key[0] !== "$") {
                    $scope[key] = null;
                }
            }
            while (watchers.length > 0) {
                watchers.shift()();
            }
            watchers = null;
            win = null;
            tpl = null;
        }

        win.on('resize', $scope.resizePopup);
        //watchers.push($rootScope.$on('open_preview_popup', onOpenPreviewPopupHandler));
        watchers.push($rootScope.$on('open_ng_popup', onOpenPopupHandler));
        watchers.push($rootScope.$on('close_ng_popup', onClosePopupHandler));
        watchers.push($rootScope.$on('resizePopup', $scope.resizePopup));
        watchers.push($scope.$on('$destroy', onDestroy));
    }])
    .directive('popup', [function () {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            controller: 'PopupController',
            template: '<div id="curtain" class="hidden"><div></div></div>',
            link: function (scope, element) {
                var prevNh,
                    unDestroy,
                    unWatch;

                function onOffsetHeightChange() {
                    var child = element.children()[0],nh = child ? child.offsetHeight: 0;
                    if (nh && nh !== prevNh) {
                        scope.resizePopup();
                        prevNh = nh;
                    }
                }

                function onDestroy() {
                    unWatch();
                    unDestroy();

                    onOffsetHeightChange = null;
                    onDestroy = null;

                    unWatch = null;
                    unDestroy = null;
                }

                unWatch = scope.$watch(onOffsetHeightChange);
                unDestroy = scope.$on('$destroy', onDestroy);
            }
        };
    }])
    .directive('deleteVariantPopup', function () {
        return {
            restrict: 'E',
            replace: true,
            //language=HTML
            template: '<div class="ngpopup">\n    <div class="delete-question-popup">\n        <div class="header">\n            <h3 class="text">Несткунии Вариант</h3>\n        </div>\n        <div class="delete-question-body">\n            <div>Шумо дар ҳақиқат ин варианти донишҷуро нест кардан мехоҳед?</div>\n        </div>\n        <div class="delete-question-footer">\n            <button class="cw-button cw-green-small-button" ng-click="onSubmitDeleteVariant()">\n                <span class="icon-check"></span>\n                <span>ҲА</span>\n            </button>\n            <button class="cw-button cw-maroon-small-button" ng-click="onCancelPopup()">\n                <span class="icon-crest"></span>\n                <span>НЕ</span>\n            </button>\n        </div>\n    </div>\n</div>'
        }
    })
    .directive('deleteTestPopup', function () {
        return {
            restrict: 'E',
            replace: true,
            //language=HTML
            template: '<div class="ngpopup">\n    <div class="delete-question-popup">\n        <div class="header">\n            <h3 class="text">Несткунии Ведемост</h3>\n        </div>\n        <div class="delete-question-body">\n            <div>Шумо дар ҳақиқат ин ведемостро нест кардан мехоҳед?</div>\n        </div>\n        <div class="delete-question-footer">\n            <button class="cw-button cw-green-small-button" ng-click="onSubmitDeleteTest()">\n                <span class="icon-check"></span>\n                <span>ҲА</span>\n            </button>\n            <button class="cw-button cw-maroon-small-button" ng-click="hidePopup()">\n                <span class="icon-crest"></span>\n                <span>НЕ</span>\n            </button>\n        </div>\n    </div>\n</div>'
        }
    });