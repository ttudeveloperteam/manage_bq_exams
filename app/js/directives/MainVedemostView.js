angular.module('mainVedemostView', ['vedemostView', 'vedemostTestLinkView', 'VedemostService'])
    .directive('mainVedemostView', ['$rootScope', 'VedemostService', function ($rootScope, VedemostService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                user: '=?',
                userFaculty: '=?',
                isFullAccess: '=?',
                isTrimester: '=?',
                isVisible: '=?'
            },
            template: '<div>\n   <vedemost-view ng-show="isVedemostView" is-visible="isVedemostView" user="user" user-faculty="userFaculty" is-full-access="isFullAccess"></vedemost-view>\n   <vedemost-test-link-view ng-if="isVedemostLinkView" is-visible="isVedemostLinkView"></vedemost-test-link-view>\n</div>',
            link: function (scope) {
                var watchers = [];
                scope.isVedemostView = false;
                scope.isVedemostLinkView = false;

                function onDestroy(){
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    watchers = null;
                }


                function onIsVisibleChange(newValue, oldValue){
                    if(newValue) {
                        scope.isVedemostView = true;
                        scope.isVedemostLinkView = false;
                    } else {
                        scope.isVedemostView = false;
                        scope.isVedemostLinkView = false;
                    }
                    $rootScope.$$phase || scope.$apply();
                }

                function onUserChange(newValue, oldValue){
                    if(newValue){
                        VedemostService.initConfig(scope.user, scope.userFaculty);
                    }
                }

                function onUserFacultyChange(newValue, oldValue){
                    if(newValue){
                        VedemostService.initConfig(scope.user, scope.userFaculty);
                    }
                }

                function onShowVedemostView(){
                    if(scope.isVisible) {
                        scope.isVedemostView = true;
                        scope.isVedemostLinkView = false;
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                function onShowVedemostLinkView(event, vedemost){
                    if(scope.isVisible) {
                        scope.currentVedemost = VedemostService.currentVedemost = vedemost;
                        scope.isVedemostView = false;
                        scope.isVedemostLinkView = true;
                        $rootScope.$$phase || scope.$apply();
                    }
                }


                watchers.push(scope.$watch('user', onUserChange));
                watchers.push(scope.$watch('userFaculty', onUserFacultyChange));
                watchers.push(scope.$watch('isVisible', onIsVisibleChange));
                watchers.push($rootScope.$on('showVedemostView', onShowVedemostView));
                watchers.push($rootScope.$on('showVedemostLinkView', onShowVedemostLinkView));
                watchers.push($rootScope.$on('$destroy', onDestroy));
            }
        }
    }]);