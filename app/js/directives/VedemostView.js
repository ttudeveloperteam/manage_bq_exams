angular.module('vedemostView', ['leftSide'])
    .directive('vedemostView', ['$rootScope', 'VedemostService', 'DataService', function ($rootScope, VedemostService, DataService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                user: '=?',
                userFaculty: '=?',
                isFullAccess: '=?',
                isVisible: '=?'
            },
            template: '<div class="vedemost-view">\n    <div class="vedemost-left-side">\n        <left-side is-visible="isVisible" \n                   user="user" \n                   user-faculty="userFaculty"\n                ></left-side>\n    </div>\n    <div class="vedemost-right-side">\n        <table class="vedemost-main-table">\n            <tbody>\n            <tr>\n                <td class="vedemost-cl0"><span><strong>№</strong></span></td>\n                <td style="width: 40%;"><span><strong>Номи Фан</strong></span></td>\n                <td style="width: 8%;text-align: center;"><span><strong>Курс</strong></span></td>\n                <td style="width: 8%;text-align: center;"><span><strong>Гуруҳ</strong></span></td>\n                <td style="width: 8%;text-align: center;"><span><strong>Нимсола</strong></span></td>\n                <td style="width: 31%;text-align: center;"><span><strong>Амалиёт</strong></span></td>\n            </tr>\n            <tr ng-repeat="vedemost in vedemosts">\n                <td class="vedemost-cl0">{{$index+1}}</td>\n                <td style="width: 40%;" ng-bind="vedemost.subject_name + \'  (\' + vedemost.vd_num_ved +\')\'" ng-class="{\'open-vedemost\':vedemost.isOpened == 1}"></td>\n                <td style="width: 8%;text-align: center;"><span ng-bind="vedemost.vd_course"></span></td>\n                <td style="width: 8%;text-align: center;"><span ng-bind="vedemost.vd_group"></span></td>\n                <td style="width: 8%;text-align: center;"><span ng-bind="vedemost.vd_polyear"></span></td>\n                <td style="width: 31%;text-align: center;">\n                    <span  class="vedemost-table-controls icon-pencil" ng-click="onLinkButtonClickHandler(vedemost)" ng-show="isFullAccess"></span>\n                    <img class="vedemost-lock-btn" src="img/unlock-icon.png" ng-show="vedemost.st_status == 0" alt="Имтиҳон насупоридаст"/>\n                    <img class="vedemost-lock-btn" src="img/work_in_progress.png" ng-show="vedemost.st_status == 1"\n                         ng-click="onUnlockInprogressStudent(vedemost)"  alt="Имтиҳон супорида истодааст"/>\n                    <img class="vedemost-lock-btn"\n                         src="img/lock-icon.png"\n                         ng-show="vedemost.st_status == 4"\n                         ng-class="{\'disable-btn\': vedemost.vd_khol_bq > 0}"\n                         ng-click="onUnlockTestButtonClickHandler(vedemost)" alt="Имтиҳон супоридааст"/>\n                    <a href="" ng-click="onRemoveVariant(vedemost)" ng-show="vedemost.st_status == 0">Несткунии Вариант</a>\n                </td>\n            </tr>\n            </tbody>\n        </table>\n    </div>\n</div>',
            link: function (scope) {
                var student, watchers = [], lastPolugodie = 1;
                scope.polugodie = 1;
                scope.currentVedemost = null;

                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    watchers = null;
                }


                function onPolugodieChange(event, polugodie) {
                    lastPolugodie = scope.polugodie;
                    scope.polugodie = polugodie;
                    // onTreeMenuSelectionChange(null, course, group);
                }

                function onTreeMenuSelectionChange(event, inStudent) {
                    if (scope.isVisible && inStudent) {
                        student = inStudent;
                        scope.vedemosts = null;
                        $rootScope.$$phase || scope.$apply();
                        DataService.getStudentBqVedemosts(student);
                    }
                }

                function onVedemostsLoaded() {
                    if (scope.isVisible) {
                        scope.vedemosts = VedemostService.vedemosts;
                        $rootScope.$$phase || scope.$apply();
                    }
                }


                function onIsVisibleChange(newValue, oldValue) {
                    if (newValue != oldValue) {
                        if (newValue) {
                            if (lastPolugodie != scope.polugodie) {
                                onTreeMenuSelectionChange(null, student);
                            }
                        }
                    }
                }

                function onStudentStatusUpdated(){
                    if(scope.isVisible && scope.currentVedemost) {
                        scope.currentVedemost.st_status = 0;
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                function onDeleteVariantConfirm(){
                    if(scope.isVisible && scope.currentVedemost){
                        VedemostService.removeStudentVariant(scope.currentVedemost.vd_IDstud_bq, scope.currentVedemost.id_bq, scope.currentVedemost.exam_type, scope.currentVedemost.vd_polyear, scope.currentVedemost.vd_ved_type);
                    }
                }

                scope.onLinkButtonClickHandler = function (vedemost) {
                    $rootScope.$emit('showVedemostLinkView', vedemost);
                };


                scope.onUnlockInprogressStudent = function(vedemost){
                    if(scope.isVisible) {
                        scope.currentVedemost = vedemost;
                        DataService.updateStudentBQVedemostStatus(vedemost.id_bq, 0);
                    }
                };

                scope.onUnlockTestButtonClickHandler = function(vedemost){
                    if(+vedemost.score > 0){
                        return;
                    }
                    if(scope.isFullAccess) {
                        scope.currentVedemost = vedemost;
                        DataService.updateStudentBQVedemostStatus(vedemost.id_bq, 0);
                    } else {
                        $rootScope.$emit('open_ng_popup', 'infoPopup', 'Ба сардори офиси бақайдгиранда муроҷиат намоед.');
                    }
                };

                scope.onRemoveVariant = function(vedemost){
                    scope.currentVedemost = vedemost;
                    $rootScope.$emit('open_ng_popup', 'deleteVariantPopup');
                };



                watchers.push(scope.$watch('isVisible', onIsVisibleChange));
                watchers.push($rootScope.$on('treeDataLoaded', function(){
                    scope.vedemosts = VedemostService.vedemosts = [];
                }));
                watchers.push($rootScope.$on('treeMenuSelectionChange', onTreeMenuSelectionChange));
                watchers.push($rootScope.$on('vedemostsLoaded', onVedemostsLoaded));
                watchers.push($rootScope.$on('polugodieChange', onPolugodieChange));
                watchers.push($rootScope.$on('deleteVariantConfirm', onDeleteVariantConfirm));
                watchers.push($rootScope.$on('studentStatusUpdated', onStudentStatusUpdated));
                watchers.push($rootScope.$on('$destroy', onDestroy));
            }
        }
    }]);