angular.module('manageExamsView', ['loadingBar'])
    .directive('manageExamsView', ['$rootScope', 'DataService', function ($rootScope, DataService) {
        return {
            restrict: 'E',
            replace: true,
            template: '<div>\n   <popup direction="vertical"></popup>\n   <main-vedemost-view is-visible="true" is-full-access="isFullAccess" user="user" user-faculty="userFaculty"></main-vedemost-view>\n    <loading-bar></loading-bar>\n</div>',
            link: function (scope, elem) {
                var watchers = [];

                scope.viewIndex = 1;
                scope.isFullAccess = true;
                scope.isTrimester = true;
                scope.user = DataService.teacherId;
                scope.userName = DataService.teacherName;
                scope.userFaculty = DataService.teacherFaculty;
                $rootScope.$$phase || scope.$apply();
                scope.polugodie = 1;

                function generateVO() {
                    var collection = [], vo;
                    for (var i = 1; i < 3; i++) {
                        vo = {};
                        vo.value = i;
                        vo.title = "Нимсолаи " + i;
                        collection.push(vo);
                    }
                    return collection;
                }

                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    watchers = null;
                }

                scope.onViewIndexChange = function (index) {
                    scope.viewIndex = index;
                    $rootScope.$$phase || scope.$apply();
                };

                scope.polugodieChange = function () {
                    $rootScope.$emit('polugodieChange', scope.polugodie);
                };

                watchers.push($rootScope.$on('$destroy', onDestroy));

                //scope.polugodie = generateVO()[0];
                //scope.polugodie = scope.polugodieVO[0];
                //scope.polugodieVO = generateVO();
                //$rootScope.$$phase || scope.$apply();
                scope.polugodieChange();
            }
        }
    }]);