angular.module('leftSide', ['treeView', 'LeftSideService'])
    .directive("leftSide", ['$rootScope', 'locale', 'LeftSideService', function ($rootScope, locale, LeftSideService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                isVisible: '=?',
                user: '=?',
                userFaculty: '=?'
            },
            template: '<div class="left-side-menu-container">\n    <div class="left-side-combobox-container">\n        <form class="form-horizontal" role="form">\n            <div class="form-group">\n                <label for="faculty" class="col-sm-12">Факултет</label>\n                <div class="col-sm-12">\n                    <select id="faculty" ng-disabled="isDisableb" \n                            class="select-box" \n                            title="{{locale.selectFinanceTypeToolTip}}" \n                            ng-model="currentFaculty"\n                            ng-options="faculty.shortName for faculty in faculties track by faculty.id">\n                    </select>\n                </div>\n            </div>\n            \n            <div class="form-group pagination-centered centered-div" style="padding-top: 10px;">\n                <button class="btn  btn-success" \n                        title="{{locale.sendRequestBtnToolTip}}" ng-click="getCoursesClicked()"><span>&gt;&gt;&gt;</span></button>\n            </div>\n        </form>\n    </div>\n\n    <tree-view tree-model="dataProvider" class="tree-menu-container"></tree-view>\n</div>\n',
            link: function (scope) {
                //---------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------
                var watchers = [], vo;
                vo = new FacultyVO();
                vo.shortName = "Ҳама";
                vo.id = "*";
                //---------------------------------------------------------
                // Scope Variables
                //---------------------------------------------------------
                scope.dataProvider = null;
                scope.isDisableb = false;

                scope.faculties = null;

                scope.financeTypes = [
                    {id: -1, label: 'Буҷавӣ'},
                    {id: 0, label: 'Шартномавӣ'}
                ];

                scope.departments = [
                    {id: 1, label: 'Рӯзона'},
                    {id: 2, label: 'Ғоибона'}
                ];

                scope.learningLanguages = [
                    {id: 3, label: 'Тоҷикӣ'},
                    {id: 4, label: 'Русӣ'}
                ];

                scope.currentFaculty = vo;
                scope.currentFinanceType = "*";
                scope.currentDepartment = "*";
                scope.currentLanguage = "*";
                //---------------------------------------------------------
                // Private Methods
                //---------------------------------------------------------
                /**
                 *
                 * @param event
                 */
                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    resourceChanged = null;
                    watchers = null;
                }

                /**
                 * Locale file loaded handler
                 */
                function resourceChanged() {
                    scope.locale = locale.props;
                    $rootScope.$$phase || scope.$apply();
                }


                function getSelectedOption(faculty_id) {
                    for (var i = 0; i < scope.faculties.length; i++) {
                        if (scope.faculties[i].id == faculty_id) {
                            return scope.faculties[i];
                        }
                    }
                }

                function onFacultiesLoaded() {
                    if (scope.isVisible) {
                        scope.faculties = [vo].concat(LeftSideService.faculties);
                        scope.currentFaculty = scope.userFaculty > 0 ? getSelectedOption(scope.userFaculty) : vo;
                        scope.getCoursesClicked();
                        scope.isDisableb = scope.userFaculty > 0;
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                function onTreeDataLoaded() {
                    if (scope.isVisible) {
                        scope.dataProvider = LeftSideService.treeDataProvider;
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                function onCourseSelected(event, course) {
                    if (scope.isVisible) {
                        scope.course = course;
                    }
                }

                function onIsVisibleChange(newValue) {
                    if (newValue && !scope.dataProvider) {
                        LeftSideService.getAllFaculties();
                    }
                }

                function onUserChange(newValue, oldValue) {
                    if (newValue) {
                        LeftSideService.httpOptions.headers = LeftSideService.httpOptions.headers || {};
                        LeftSideService.httpOptions.headers.user = scope.user;
                    }
                }

                function onUserFacultyChange(newValue) {
                    LeftSideService.teacherFaculty = newValue;
                }

                //---------------------------------------------------------
                // Public Methods
                //---------------------------------------------------------
                scope.getCoursesClicked = function () {
                    if (scope.isVisible) {
                        LeftSideService.getCoursesAndGroups(scope.currentFaculty.id, scope.currentDepartment);
                    }
                };

                watchers.push(scope.$watch('user', onUserChange));
                watchers.push(scope.$watch('userFaculty', onUserFacultyChange));
                watchers.push(scope.$watch('isVisible', onIsVisibleChange));
                watchers.push(scope.$on('$destroy', onDestroy));
                watchers.push($rootScope.$on('localeLoaded', resourceChanged));
                watchers.push($rootScope.$on('facultiesLoaded', onFacultiesLoaded));
                watchers.push($rootScope.$on('treeDataLoaded', onTreeDataLoaded));
                watchers.push($rootScope.$on('courseSelected', onCourseSelected));
                resourceChanged();

            }
        };
    }]);