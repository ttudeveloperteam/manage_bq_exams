angular.module('vedemostTestLinkView', ['ngSanitize', 'ui.select'])
    .filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                items.forEach(function (item) {
                    var itemMatches = false;

                    var keys = Object.keys(props);
                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    })
    .directive('vedemostTestLinkView', ['$rootScope', 'VedemostService', 'DataService', function ($rootScope, VedemostService, DataService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                isVisible: '=?'
            },
            //language=HTML
            template: '<div style="margin: 0 18px;">\n    <div class="vedemost-test-content">\n        <div class="vedemost-view-row">\n            <span>\n            <span >Номи Фан:</span>\n            <span ng-bind="vedemost.subject_name"></span>\n            </span>\n            <span>\n            <span >Коди Гуруҳ:</span>\n            <span ng-bind="vedemost.vd_group"></span>\n                 </span>\n           \n        </div>\n        <div class="vedemost-settings-holder">\n            <div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Тест:</span>\n                <ui-select ng-model="test" theme="select2"  class="select2-main-styles" on-select="onChange($select.selected)">\n                    <ui-select-match placeholder="Тестро Интихоб Кунед">{{$select.selected.description}}</ui-select-match>\n                    <ui-select-choices group-by="\'subject_name\'" repeat="item in tests  | propsFilter: {description: $select.search, teacher_name:$select.search, subject_name:$select.search}">\n                        <div ng-bind-html="item.description  | highlight: $select.search"></div>\n                        <small>\n                            <i><strong><u>Омӯзгор:</u></strong> <span ng-bind-html="\'\'+item.teacher_name | highlight: $select.search"></span></i>\n                            <i><strong><u>Фан:</u></strong> <span ng-bind-html="\'\'+item.subject_name | highlight: $select.search"></span></i>\n                        </small>\n                    </ui-select-choices>\n                </ui-select>\n            </div>\n            <div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Профил:</span>\n                <select class="vedemost-ddlist" ng-model="profile" ng-options="item.profile_name for item in profiles track by item.profile_id">\n                </select>\n            </div>\n            <div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Санҷиш:</span>\n                <select class="vedemost-ddlist" ng-model="examType" ng-options="item.title for item in examTypes track by item.value">\n                </select>\n               \n            </div>\n            <div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Дарозноки:</span>\n                <input class="vedemost-ddlist"\n                       ng-model="testTime"\n                       type="number" min="1" max="60"/>\n            </div>\n            <div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Биометрик:</span>\n                <input class="vedemost-ddlist"\n                       style="width: 40px;"\n                       ng-model="vedemost.isBiometric"\n                       type="checkbox"/>\n            </div>\n            <div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Кушода:</span>\n                <input class="vedemost-ddlist"\n                       style="width: 40px;"\n                       ng-model="vedemost.isOpened"\n                       type="checkbox"/>\n            </div><!--\n            <div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Иҷозат:</span>\n                <input class="vedemost-ddlist"\n                       style="width: 40px;"\n                       ng-model="vedemost.hasApproval"\n                       type="checkbox"/>\n            </div>-->\n            <!--<div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Оғоз:</span>\n                <input class="vedemost-ddlist"\n                       style="width: 340px;"\n                       ng-model="vedemost.start_date"\n                       type="datetime-local"/>\n            </div>\n            <div class="vedemost-crt-test-row" style="border-bottom: 1px solid #C2B9B9;">\n                <span >Анҷом:</span>\n                <input class="vedemost-ddlist"\n                       style="width: 340px;"\n                       ng-model="vedemost.end_date"\n                       type="datetime-local"/>\n            </div>-->\n            <div class="vedemost-answers-container" style="position: relative;">\n                <table class="vedemost-main-table">\n                    <tbody style="overflow: hidden;">\n                    <tr>\n                        <td class="vedemost-cl0"><span><strong>№</strong></span></td>\n                        <td style="width: 58%;"><span><strong>Номи Насаб</strong></span></td>\n                    </tr>\n                    <tr ng-repeat="student in students">\n                        <td class="vedemost-cl0">{{$index+1}}\n                            <input style="width: 25px; height: 20px;"\n                                   ng-model="student.selected"\n                                   type="checkbox"/>\n                        </td>\n                        <td style="width: 58%;" ng-bind="student.FioTj"></td>\n                    </tr>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </div>\n\n    \n    <div class="vedemost-view-footer">\n        <span ng-show="isError" style="color: red;font-size: 18px;padding: 0 20px;">Арсаҳоро пур кунед!!!</span>\n        <button style="height: 44px;"\n                class="cw-button cw-green-medium-button"\n                ng-click="onSubmit()">\n            <span>САБТ</span>\n        </button>\n        <button style="height: 44px;" class="cw-button cw-blue-medium-button" ng-click="onReturnClicked()">\n            <span>Бозгашт</span>\n        </button>\n    </div>\n</div>',
            link: function (scope) {
                var watchers = [], vo, profileVo, examVo;
                vo = new TestVO();
                vo.description = "Тестро Интихоб Кунед";
                vo.test_id = -1;

                profileVo = {profile_id: "-1", profile_name: "Профилро Интихоб Кунед"};
                examVo = {title: "Санҷишро Интихоб Кунед", value: "-1"};

                scope.polugodie = 1;
                scope.isVedemostOpened = false;
                scope.isError = false;
                scope.vedemost = null;
                scope.tests = null;
                scope.test = vo;
                scope.testTime = 0;
                scope.profiles = null;
                scope.profile = profileVo;
                scope.examType = examVo;
                scope.examTypes = [{title: "Санҷишро Интихоб Кунед", value: "-1"},
                    {title: "Рейтинги 1", value: 1},
                    {title: "Рейтинги 2", value: 2},
                    {title: "Имтиҳони Омузгор", value: 3},
                    {title: "Имтиҳони Ҷамбстӣ", value: 4},
                    {title: "Имтиҳони Триместр", value: 5}];

                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    watchers = null;
                }


                function onIsVisibleChange() {
                    scope.test = vo;
                    scope.vedemost = VedemostService.currentVedemost;
                    scope.vedemost.isBiometric = scope.vedemost.isBiometric == '0' ? false : true;
                    scope.vedemost.isOpened = scope.vedemost.isOpened == '0' ? false : true;
                    VedemostService.getTestsByKafedraId(VedemostService.currentVedemost.vd_kafedra, scope.polugodie);
                    // VedemostService.getTestsByKafedraId(14, scope.polugodie);
                    VedemostService.getAllProfiles();
                    scope.profile = scope.vedemost.profile_id != 0 ? scope.vedemost.profile_id : -1;
                    scope.examType = scope.vedemost.exam_type != 0 ? scope.vedemost.exam_type : -1;
                    DataService.getStudentsBySubjectId(scope.vedemost.id_bq, scope.vedemost.vd_subject);
                    scope.testTime = parseInt(scope.vedemost.test_time, 10) || 20;
                }

                function onPolugodieChange(event, polugodie) {
                    scope.polugodie = polugodie;
                    if (scope.isVisible) {
                        scope.onReturnClicked();
                    }
                }

                function getSelectedOption(test_id) {
                    for (var i = 0; i < scope.tests.length; i++) {
                        if (scope.tests[i].test_id == test_id) {
                            return scope.tests[i];
                        }
                    }
                }

                function getProfileSelectedOption(profile_id) {
                    if (profile_id > 0) {
                        for (var i = 0; i < scope.profiles.length; i++) {
                            if (scope.profiles[i].profile_id == profile_id) {
                                return scope.profiles[i];
                            }
                        }
                    }
                    return profileVo;
                }

                function getExamTypeSelectedOption(examType) {
                    if (examType > 0) {
                        for (var i = 0; i < scope.examTypes.length; i++) {
                            if (scope.examTypes[i].value == examType) {
                                return scope.examTypes[i];
                            }
                        }
                    }
                    return examVo;
                }

                function onTestsLoaded() {
                    if (scope.isVisible && scope.vedemost) {
                        scope.tests = VedemostService.tests;
                        scope.test = scope.vedemost.test_id > 0 ? getSelectedOption(parseInt(scope.vedemost.test_id, 10)) : vo;
                        scope.examType = getExamTypeSelectedOption(scope.vedemost.exam_type);
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                function onProfilesLoaded() {
                    if (scope.isVisible && scope.vedemost) {
                        scope.profiles = [profileVo].concat(VedemostService.profiles);
                        scope.profile = getProfileSelectedOption(scope.vedemost.profile_id);
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                function onItemSelected(event, item) {
                    scope.test = item;
                }

                function dateToFormatedString(d) {
                    return d.getFullYear() + '-' + (+d.getMonth() + 1).toString().padStart(2, 0) + '-' +
                        d.getDate().toString().padStart(2, 0) + ' ' + d.getHours().toString().padStart(2, 0) + ':' + d.getMinutes().toString().padStart(2, 0) + ':' + d.getSeconds().toString().padStart(2, 0);
                }

                scope.onSubmit = function () {
                    if (scope.isVisible && scope.vedemost
                        && scope.test.test_id !== -1
                        && scope.profile.profile_id !== -1
                        && scope.testTime > 0) {
                        scope.vedemost.test_id = scope.test.test_id;
                        scope.vedemost.profile_id = scope.profile.profile_id;
                        scope.vedemost.exam_type = scope.examType.value;
                        scope.vedemost.test_time = scope.testTime;
                        scope.vedemost.isBiometric = scope.vedemost.isBiometric ? 1 : 0;
                        scope.vedemost.isOpened = scope.vedemost.isOpened ? 1 : 0;
                        scope.isError = false;
                        DataService.updateBQVedemostTestInfo(scope.vedemost);
                    } else {
                        scope.isError = true;
                    }
                };

                scope.onReturnClicked = function () {
                    $rootScope.$emit('showVedemostView', VedemostService.currentVedemost);
                };

                scope.onChange = function (test) {
                    scope.test = test;
                };

                scope.onLockUnlockButtonClickHandler = function () {
                    scope.isVedemostOpened = !scope.isVedemostOpened;
                    $rootScope.$$phase || scope.$apply();
                };
                onIsVisibleChange();
                // watchers.push(scope.$watch('isVisible', onIsVisibleChange));
                /*watchers.push(scope.$watch('examType', function (newValue) {
                    if (scope.vedemost) {
                        DataService.getStudentsByExamType(scope.vedemost.vd_id, newValue.value);
                    }
                }));*/
                watchers.push($rootScope.$on('testsLoaded', onTestsLoaded));
                watchers.push($rootScope.$on('profilesLoaded', onProfilesLoaded));
                watchers.push($rootScope.$on('vedemostTestInfoUpdated', scope.onReturnClicked));
                watchers.push($rootScope.$on('ddItemSelected', onItemSelected));
                watchers.push($rootScope.$on('polugodieChange', onPolugodieChange));
                watchers.push($rootScope.$on('StudentsLoaded', function (name, studs) {
                    if (scope.isVisible) {
                        studs.forEach(function (item) {
                            item.selected = true;
                        });
                        scope.students = studs;
                        $rootScope.$$phase || scope.$apply();
                    }
                }));
                watchers.push($rootScope.$on('permisionAdded', function (name) {
                    if (scope.isVisible) {
                        $rootScope.$emit('showVedemostLinkView', VedemostService.currentVedemost);
                    }
                }));
                watchers.push($rootScope.$on('$destroy', onDestroy));
            }
        }
    }]);