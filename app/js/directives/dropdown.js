angular.module('dropdownSelect', [])
    .directive('dropdownSelect', ['$document', function ($document) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                dropdownSelect: '=',
                dropdownModel: '=',
                dropdownOnChange: '&'
            },
            controller: [
                '$scope', '$element', '$attrs', function ($scope, $element, $attrs) {
                    var body;
                    $scope.labelField = $attrs.dropdownItemLabel != null ? $attrs.dropdownItemLabel : 'title';
                    this.select = function (selected) {
                         $scope.dropdownModel = selected;
                        $scope.dropdownOnChange({
                            selected: selected
                        });
                    };
                    body = $document.find("body");
                    body.bind('click', function () {
                        $element.removeClass('active');
                    });
                    $element.bind('click', function (event) {
                        event.stopPropagation();
                        $element.toggleClass('active');
                    })
                }],

            template: '<div class="wrap-dd-select">\n    <span class="selected">{{dropdownModel[labelField]}}</span>\n    <ul class="dropdown">\n        <li ng-repeat=\'item in dropdownSelect\'\n            class=\'dropdown-item\'\n            dropdown-select-item=\'item\'\n            dropdown-item-label=\'labelField\'></li>\n    </ul>\n</div> '
        };
    }])
    .directive('dropdownSelectItem', [function () {
        return {
            require: '^dropdownSelect',
            replace: true,
            scope: {
                dropdownItemLabel: '=',
                dropdownSelectItem: '='
            },
            link: function (scope, element, attrs, dropdownSelectCtrl) {
                scope.selectItem = function () {
                    if (scope.dropdownSelectItem.href) {
                        return;
                    }
                    dropdownSelectCtrl.select(scope.dropdownSelectItem);
                };
            },
            template: '<li \'>\n<a href=\'\' class=\'dropdown-item\'\n   ng-click=\'selectItem()\'>\n    {{dropdownSelectItem[dropdownItemLabel]}} </a>\n</li>'
        }
    }]);