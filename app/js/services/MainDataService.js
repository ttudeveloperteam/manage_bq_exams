angular.module('DataService', [])
    .service('DataService', ['$rootScope', '$http', '$window', 'VedemostService', function ($rootScope, $http, $window, VedemostService) {
        'use strict';
        //------------------------------------------------------------------------
        // Constants
        //------------------------------------------------------------------------
        //------------------------------------------------------------------------
        // Private variables
        //------------------------------------------------------------------------
        var service = this,
            httpOptions = {
            method: 'GET',
            url: '',
            responseType: 'json'
        };
        //------------------------------------------------------------------------
        // Public variables
        //------------------------------------------------------------------------
        /**
         *
         * @type {null}
         */
        this.config = null;
        /**
         *
         * @type {boolean}
         */
        this.isDataLoaded = false;
        /**
         * Id of test
         */
        this.currentTest;
        /**
         * Id of test
         */
        this.testId;
        /**
         * Id of test
         */
        this.teacherId;
        /**
         * Id of test
         */
        this.teacherFaculty;
        /**
         * Id of test
         */
        this.teacherName;

        /**
         * Id of test
         */
        this.teacherTime;
        /**
         * Id of test
         */
        this.teacherType;
        this.uchebYear;


        this.faculties = null;
        this.kafedra = null;
        this.subjects = null;
        this.profiles = null;
        this.tests;
        this.teachers;

        this.currentQuestion;
        this.currentProfile;
        this.baseAPIUrl="http://asu.techuni.tj";
        //------------------------------------------------------------------------
        // Functions(getters and setters)
        //------------------------------------------------------------------------
        /**
         * check response on error
         * @param data
         * @returns {*|boolean}
         */
        function isRequestDoneSuccesfuly(data) {
            var isSuccess = (data && !data.error);
            if(isSuccess){
                service.spinEnd();
            } else {
                service.spinError();
                console.log("Something wrong: "+ data.message);
            }
            return isSuccess;
        }


        function parseTest(item){
            var vo = new TestVO();
            vo.test_id = parseInt(item.test_id, 10);
            vo.teacher_id = parseInt(item.teacher_id, 10);
            vo.teacher_name = item.teacher_name;
            vo['teacher_score'] = item.teacher_score;
            vo.subject_id = parseInt(item.subject_id, 10);
            vo.subject_name = item.subject_name;
            vo.kafedra_id = parseInt(item.kafedra_id, 10);
            vo.kafedra_name = item.kafedra_name;
            vo.polugodie = parseInt(item.polugodie,10);
            vo.description = item.description;
            vo.language = item.language;
            vo.question_count = parseInt(item.question_count, 10);
            vo.locked = parseInt(item.locked, 10) != 0;
            return vo;
        }

        function parseTests(tests){
            var len = tests.length,
                arr = [];
            for (var i = 0; i < len; i++) {
                arr.push(parseTest(tests[i]));
            }
            return arr;
        }


        function loadTestssSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.tests = parseTests(response.tests);
                service.isDataLoaded = true;
                $rootScope.$emit('testsLoaded');
            }
        }

        function profilesLodedSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.profiles = response.profiles;
                for (var i = 0; i < service.profiles.length; i++) {
                    service.profiles[i].factor = +(service.profiles[i].factor);
                }
                $rootScope.$emit('profilesLoaded');
            }
        }

        function filterSubjectsByKafedra(kafedraId){
            var collection = [], item, subjectVo, subjectLen = service.subjects.length;
            for (var i = 0; i < subjectLen; i++) {
                item = service.subjects[i];
                if(item.vd_kafedra === kafedraId) {
                    subjectVo = new SubjectVO();
                    subjectVo.id = item.vd_sid;
                    subjectVo.kafedra_id = item.vd_kafedra;
                    subjectVo.name = item.vd_subject;
                    collection.push(subjectVo);
                }
            }
            return collection;
        }

        function filterKafedraByFaculty(facultyId){
            var collection = [], item, kafVo, kafLen = service.kafedra.length;
            for (var i = 0; i < kafLen; i++) {
                item = service.kafedra[i];
                if(item.kf_faculty === facultyId) {
                    kafVo = new KafedraVO();
                    kafVo.id = item.kf_id;
                    kafVo.shortName = item.kf_short_name;
                    kafVo.fullName = item.kf_full_name;
                    kafVo.subjects = filterSubjectsByKafedra(kafVo.id);
                    collection.push(kafVo);
                }
            }
            return collection;
        }

        function generateVO(){
            var collection = [], item, facVo, facLen = service.faculties.length;
            for (var i = 0; i < facLen; i++) {
                item = service.faculties[i];
                facVo = new FacultyVO()
                facVo.id = item.fID;
                facVo.shortName = item.fFac_NameTajShort;
                facVo.fullName = item.fFac_NameTaj;
                facVo.kafedra = filterKafedraByFaculty(facVo.id);
                collection.push(facVo);
            }
            service.faculties = collection;
        }

        function loadAllMainDataSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.faculties = response.faculties || [];
                service.kafedra = response.kafedra;
                service.subjects = response.subjects;
                generateVO();
                $rootScope.$emit('dataLoaded');
            }
        }


        function deleteTestSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                $rootScope.$emit('PermisionDeleted');
            } else {
                console.log('Delete Question failed');
            }
        }

        function requestError(response, status, headers, config) {
            throw new Error("Something wrong: " + response);
            console.log(response);
            service.spinError();
        }


        function parseParams() {
            var parameters = {};
            var query = $window.location.search;
            query = query.replace(/&amp;/ig, '&');
            query = query.replace('?', '');
            var params = query.split('&');
            for (var i = 0; i < params.length; i++) {
                var parts = params[i].split('=');
                parameters[parts[0]] = parts[1];
            }
            return parameters;
        }
        //------------------------------------------------------------------------
        // Private Methods
        //------------------------------------------------------------------------


        //------------------------------------------------------------------------
        // Public Methods
        //------------------------------------------------------------------------
        /**
         * Show Busy indicator
         */
        this.spinStart = function (){
            $rootScope.$emit('spinStart');
        };
        /**
         * Hide Busy indicator
         */
        this.spinEnd = function (){
            $rootScope.$emit('spinEnd');
        };
        /**
         * Show error message
         */
        this.spinError = function (){
            $rootScope.$emit('spinError');
        };

        this.initConfig = function (config) {
            if (config) {
                service.config = config;
                service.teacherId = parseInt(config.teacherId, 10);
                service.teacherFaculty = config.teacherFaculty;
                httpOptions.headers = httpOptions.headers || {};
                httpOptions.headers.user = service.teacherId;
                service.teacherName = config.teacherName;
                service.teacherType = config.teacherType;
                service.teacherTime = config.teacherTime;
                service.uchebYear = config.uchebYear;
                service.getAllMainData();
            }
        };
        /**
         *
         */
        this.getParams = function(){
            var params = parseParams();
            var ticket = Base64.decode(params['ticket']);
            params = ticket.split('$');
            return params;
        };
        /**
         * Authenticate user on server and return user vo
         * @param login
         * @param password
         */
        this.getAllMainData = function () {
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/main/MainAPI.php?request=getAllMainData';
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadAllMainDataSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.deletePermisionById = function (permisionId) {
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=removeVedemostPermision/' + permisionId;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(deleteTestSuccess)
                .error(requestError);
        };
        /**
         * Authenticate user on server and return user vo
         * @param login
         * @param password
         */
        this.getTestsBySubjectId = function (subjectId, polugodie) {
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/test/TestsAPI.php?request=getTestsBySubjectIdAndPolugodie/' + subjectId + '/' + polugodie;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadTestssSuccess)
                .error(requestError);
        };
        /**
         * Authenticate user on server and return user vo
         * @param login
         * @param password
         */
        this.getTestsByKafedraId = function (kafedraId) {
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/test/TestsAPI.php?request=getTestsByKafedraId/' + kafedraId;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadTestssSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.getAllProfiles = function(){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/profile/ProfileAPI.php?request=getAllProfiles';
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(profilesLodedSuccess)
                .error(requestError);
        };

        /**
         * Delete question by id
         * @param questionId
         */
        this.getStudentsBySubjectId = function(vedId, subjectId){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getBQStudentsBySubjectId/' + vedId + '/' + subjectId;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(function(response){
                    if (isRequestDoneSuccesfuly(response)) {
                        $rootScope.$emit('StudentsLoaded', response.students);
                    }
                })
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.getStudentBqVedemosts = function(student){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getStudentBqVedemosts/' + student.vd_IDstud_bq.replace('/','') + '/'+ student.vd_course_bq +'/'+ student.vd_group_bq.replace('/','$_$');
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(function(response){
                    if (isRequestDoneSuccesfuly(response)) {
                        VedemostService.vedemosts = response.vedemosts;
                        $rootScope.$emit('vedemostsLoaded');
                    } else {
                        console.log('failed');
                    }
                })
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.getVedemostPermisions = function(vedId){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getVedemostPermisions/' + vedId;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(function(response){
                    if (isRequestDoneSuccesfuly(response)) {
                        $rootScope.$emit('PermisionsLoaded', response.vedemosts);
                    }
                })
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.getVedemostPermisionStudents = function(permId){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getVedemostPermisionStudents/' + permId;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(function(response){
                    if (isRequestDoneSuccesfuly(response)) {
                        $rootScope.$emit('PermisionStudentsLoaded', response.students);
                    }
                })
                .error(requestError);
        };

        /**
         */
        this.addVedemostPermision = function(permision){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=addVedemostPermision';
            httpOptions.method = "POST";
            httpOptions.headers['Content-Type']= 'text/plain';
            httpOptions.data = JSON.stringify(permision);
            $http(httpOptions)
                .success(function(response){
                    if (isRequestDoneSuccesfuly(response)) {
                        $rootScope.$emit('permisionAdded');
                    }
                })
                .error(requestError);
        };
        /**
         */
        this.updateVedemostPermision = function(permision){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=updateVedemostPermision';
            httpOptions.method = "POST";
            httpOptions.headers['Content-Type']= 'text/plain';
            httpOptions.data = JSON.stringify(permision);
            $http(httpOptions)
                .success(function(response){
                    if (isRequestDoneSuccesfuly(response)) {
                        $rootScope.$emit('permisionUpdated');
                    }
                })
                .error(requestError);
        };
        /**
         */
        this.updateBQVedemostTestInfo = function(vedemost){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=updateBQVedemostTestInfo';
            httpOptions.method = "POST";
            httpOptions.headers['Content-Type']= 'text/plain';
            httpOptions.data = JSON.stringify(vedemost);
            $http(httpOptions)
                .success(function(response){
                    if (isRequestDoneSuccesfuly(response)) {
                        $rootScope.$emit('vedemostTestInfoUpdated');
                    }
                })
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.updateStudentBQVedemostStatus = function(vedemostId, status){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=updateStudentBQVedemostStatus';
            httpOptions.method = "POST";
            httpOptions.headers['Content-Type']= 'text/plain';
            httpOptions.data = JSON.stringify({'vedemostId':vedemostId, 'status':status});
            $http(httpOptions)
                .success(function(response){
                    if (isRequestDoneSuccesfuly(response)) {
                        $rootScope.$emit('studentStatusUpdated');
                    } else {
                        console.log('failed');
                    }
                })
                .error(requestError);
        };
    }]);