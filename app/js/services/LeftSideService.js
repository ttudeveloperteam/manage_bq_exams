angular.module('LeftSideService', [])
    .service('LeftSideService', ['$rootScope', '$http', function ($rootScope, $http) {
        'use strict';
        //------------------------------------------------------------------------
        // Constants
        //------------------------------------------------------------------------
        //------------------------------------------------------------------------
        // Private variables
        //------------------------------------------------------------------------
        var service = this;
        //------------------------------------------------------------------------
        // Public variables
        //------------------------------------------------------------------------
        this.httpOptions = {
            method: 'GET',
            url: '',
            responseType: 'json'
        };
        this.faculties = null;
        this.treeDataProvider = null;
        this.baseAPIUrl="http://asu.techuni.tj";
        //------------------------------------------------------------------------
        // Functions(getters and setters)
        //------------------------------------------------------------------------
        /**
         * check response on error
         * @param data
         * @returns {*|boolean}
         */
        function isRequestDoneSuccesfuly(data) {
            var isSuccess = (data && !data.error);
            if (!isSuccess) {
                service.spinError();
                console.log("Something wrong: " + data.message);
            } else {
                service.spinEnd();
            }
            return isSuccess;
        }

        function parseFaculty(item){
            var vo = new FacultyVO();
            vo.id = parseInt(item.fID, 10);
            vo.shortName = item.fFac_NameTajShort;
            vo.fullName = item.fFac_NameTaj;
            return vo;
        }

        function parseFaculties(faculties){
            var len = faculties.length,
                arr = [];
            for (var i = 0; i < len; i++) {
                arr.push(parseFaculty(faculties[i]));
            }
            return arr;
        }

        function filterKafedraByFaculty(facultyId){
            var collection = [], item, kafVo, kafLen = service.kafedra.length;
            for (var i = 0; i < kafLen; i++) {
                item = service.kafedra[i];
                if(item.kf_faculty === facultyId) {
                    kafVo = new KafedraVO();
                    kafVo.id = item.kf_id;
                    kafVo.shortName = item.kf_short_name;
                    kafVo.fullName = item.kf_full_name;
                    kafVo.subjects = filterSubjectsByKafedra(kafVo.id);
                    collection.push(kafVo);
                }
            }
            return collection;
        }


        function loadAllFacultiesSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.faculties = parseFaculties(response.faculties);
                $rootScope.$emit('facultiesLoaded');
            } else {
                console.log('faculties Loaded failed');
            }
        }

        function parseGroups(groups, course){
            var arr = [],
                group,
                len = groups.length;
            for (var i = 0; i < len; i++) {
                group = groups[i];
                if (group.length > 6) {
                    arr.push({label: group, course:course});
                }
            }
            return arr;
        }

        function parseTreeData(students){
            var arr = [],
                treeItem,
                map = {};
            students.forEach(function(item){
                if(!map.hasOwnProperty('' + item.vd_course_bq)){
                    map['' + item.vd_course_bq] = [];
                }
                map['' + item.vd_course_bq].push(item);
            });
            Object.keys(map).forEach(function(key){
                treeItem = { label : "Курси-"+ key, course: key, children : map[key].map(function(item){return {label: item.FioTj, course:item}}), collapsed:true};
                arr.push(treeItem);
            });
            return arr;
        }

        function loadCoursesAndGroupsSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.treeDataProvider = parseTreeData(response.students);
                $rootScope.$emit('treeDataLoaded');
            } else {
                console.log('faculties Loaded failed');
            }
        }

        function requestError(response, status, headers, config) {
            throw new Error("Something wrong: " + response);
            console.log(response);
            service.spinError();
        }

        //------------------------------------------------------------------------
        // Private Methods
        //------------------------------------------------------------------------


        //------------------------------------------------------------------------
        // Public Methods
        //------------------------------------------------------------------------
        /**
         * Show Busy indicator
         */
        this.spinStart = function (){
            $rootScope.$emit('spinStart');
        };
        /**
         * Hide Busy indicator
         */
        this.spinEnd = function (){
            $rootScope.$emit('spinEnd');
        };
        /**
         * Show error message
         */
        this.spinError = function (){
            $rootScope.$emit('spinError');
        };

        /**
         * Authenticate user on server and return user vo
         * @param login
         * @param password
         */
        this.getAllFaculties = function () {
            service.spinStart();
            service.httpOptions.url = service.baseAPIUrl + '/api/v1/main/MainAPI.php?request=getAllFaculties';
            service.httpOptions.method = "GET";
            $http(service.httpOptions)
                .success(loadAllFacultiesSuccess)
                .error(requestError);
        };

        /**
         * Authenticate user on server and return user vo
         * @param login
         * @param password
         */
        this.getCoursesAndGroups = function (facultyId, departmentId) {
            service.spinStart();
            service.httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getBqStudents/'+ facultyId + '/' +departmentId;
            service.httpOptions.method = "GET";
            $http(service.httpOptions)
                .success(loadCoursesAndGroupsSuccess)
                .error(requestError);
        };

    }]);