module.exports = function (grunt) {
    grunt.initConfig({
            pkg: grunt.file.readJSON('package.json') || grunt.fatal('package.json not found'),
            meta: {
                banner: '/*\n* Copyright <%= grunt.template.today("yyyy") %>, Transparent Language, Inc..\n' +
                '* All Rights Reserved.\n' +
                '* TTU Confidential Information\n*/\n'
            },
            bowerDir: 'bower_components',
            appDir: 'app',
            version: '1.0.0.',
            concat: {
                js: {
                    options: {
                        separator: "\n"
                    },
                    files: [
                        {
                            src: [
                                'banner:meta.banner',

                                'app/<%=bowerDir%>/coretypes/src/vo/TestVO.js',
                                'app/<%=bowerDir%>/coretypes/src/vo/TeacherConfigVO.js',
                                'app/<%=bowerDir%>/coretypes/src/util/ArrayUtil.js',
                                'app/<%=bowerDir%>/coretypes/src/util/ModuleUtil.js',
                                'app/<%=bowerDir%>/coretypes/src/services/LocaleService.js',

                                'app/<%=bowerDir%>/components2/src/js/utils/Base64.js',
                                'app/<%=bowerDir%>/components2/src/js/utils/PlatformUtil.js',
                                'app/<%=bowerDir%>/components2/src/js/directives/ScaleToFit.js',
                                'app/<%=bowerDir%>/components2/src/js/directives/TreeViewItem.js',
                                'app/<%=bowerDir%>/components2/src/js/directives/TreeView.js',
                                'app/<%=bowerDir%>/components2/src/js/directives/LoadingBar.js',

                                'app/<%=bowerDir%>/components2/src/js/services/VedemostService.js',


                                'app/<%=bowerDir%>/coretypes/src/constants/MessageConstants.js',
                                'app/<%=bowerDir%>/coretypes/src/constants/QuestionEvents.js',
                                'app/<%=bowerDir%>/coretypes/src/util/JunctionUtil.js',
                                'app/<%=bowerDir%>/coretypes/src/vo/FacultyVO.js',
                                'app/<%=bowerDir%>/coretypes/src/vo/KafedraVO.js',
                                'app/<%=bowerDir%>/coretypes/src/vo/SubjectVO.js',
                                'app/<%=bowerDir%>/coretypes/src/vo/SettingVO.js',
                                'app/js/**/*.js'
                            ],
                            dest: 'app/bin/js/manageBQExams.min.js'
                        }
                    ]
                }
            },
            uglify: {
                options: {
                    banner: '<%= meta.banner %>',
                    mangle: false
                },
                js: {
                    files: [
                        {
                            'app/bin/js/manageBQExams.min.js': ['app/bin/js/manageBQExams.min.js']
                        }
                    ]
                }
            },
            compass: {
                clean: {
                    options: {
                        clean: true
                    }
                },
                dev: {
                    options: {
                        sassDir: ['app/sass'],
                        cssDir: ['app/css'],
                        environment: 'development',
                        force: true
                    }
                },
                prod: {
                    options: {
                        sassDir: ['app/sass'],
                        cssDir: ['app/bin/css'],
                        environment: 'production',
                        force: true
                    }
                },
                files: ['app/sass/**/*.scss']
            },
            copy: {
                release: {
                    files: [
                        {expand: true, cwd: 'app/img/', src: ['**'], dest: 'app/bin/img/'},
                        {
                            expand: true,
                            cwd: 'app/<%=bowerDir%>/components2/src/img/',
                            src: ['**'],
                            dest: 'app/bin/img/'
                        },
                        {expand: true, cwd: 'app/template/', src: ['**'], dest: 'app/bin/template/'},
                        {
                            expand: true,
                            cwd: 'app/<%=bowerDir%>/angular/',
                            src: ['angular.min.js'],
                            dest: 'app/bin/vendor/'
                        },
                        {
                            expand: true,
                            cwd: 'app/<%=bowerDir%>/angular-touch/',
                            src: ['angular-touch.min.js'],
                            dest: 'app/bin/vendor/'
                        },
                        {
                            expand: true,
                            cwd: 'app/<%=bowerDir%>/angular-sanitize/',
                            src: ['angular-sanitize.min.js'],
                            dest: 'app/bin/vendor/'
                        },
                        {
                            expand: true,
                            cwd: 'app/<%=bowerDir%>/components2/src/vendor/css/',
                            src: ['**'],
                            dest: 'app/bin/vendor/css'
                        },
                        {
                            expand: true,
                            cwd: 'app/vendor/fonts',
                            src: ['**'],
                            dest: 'app/bin/vendor/fonts'
                        },
                        {
                            expand: true,
                            cwd: 'app/<%=bowerDir%>/components2/src/vendor/js/',
                            src: ['**'],
                            dest: 'app/bin/vendor/js'
                        }
                    ]
                }
            },
            preprocess: {
                options: {
                    context: {
                        VERSION: "" + Date.now()
                    }
                },
                html: {
                    src: 'app/manage_bq_exams.html',
                    dest: 'app/bin/manage_bq_exams.html'
                }
            },
            jshint: {
                options: {
                    curly: true,
                    eqeqeq: true,
                    eqnull: true,
                    browser: true
                },
                files: ['Gruntfile.js', 'app/js/**/*.js']
            },
            watch: {
                js: {
                    files: ['public_html/js/**/*.js'],
                    tasks: ['concat:js', 'uglify:js', 'jshint']
                }
            },
            clean: {
                build: {
                    src: ['app/bin/', 'dist', 'app/<%=bowerDir%>/']
                }
            },
            shell: {
                bowerCacheClean: {
                    options: {
                        stdout: true
                    },
                    command: 'bower cache clean'
                },
                bowerInstall: {
                    options: {
                        stdout: true
                    },
                    command: 'bower install'
                }
            }
        }
    );
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-preprocess');
    grunt.registerTask('build', [/*'clean', 'shell:bowerCacheClean', 'shell:bowerInstall',*/ 'concat', 'uglify', 'compass:prod', 'preprocess', 'copy:release']);
};


